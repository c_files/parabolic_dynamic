set terminal postscript portrait enhanced mono dashed lw 1 "Helvetica" 14     
set output "data1.ps"
set title "Points on dynamical plane "
set ylabel "ratio"
set xlabel "iteratio max"
#set xrange [0:150]
# define styles 
#set style data lines
set style line 1 lc rgb "green" lt 1 lw 5 pt 1 ps 1.5   # 
set style line 2 lc rgb "blue"  lt 1 lw 5 pt 1 ps 1.5   # 
set style line 3 lc rgb "red"   lt 1 lw 5 pt 1 ps 1.5   # 
# plot "data.txt" using 1:2 title 'exterior' lc 1,  "data.txt" using 1:3 title 'interior' lc 2, "data.txt" using 1:4 title 'unknown' lc 3
# plot "data.txt" using 1:2 title 'exterior' with linespoints ls 1, "data.txt" using 1:3 title 'interior' with linespoints ls 2, "data.txt" using 1:4 title 'unknown' with linespoints ls 3
# To reuse the last filename we can just type ""
# plot "data.txt" using 1:2 title 'exterior' with linespoints ls 1, "" using 1:3 title 'interior' with linespoints ls 2, "" using 1:4 title 'unknown' with linespoints ls 3
plot "data.txt" using 1:2 title 'exterior' with lines ls 1, "" using 1:3 title 'interior' with lines ls 2, "" using 1:4 title 'unknown' with lines ls 3

