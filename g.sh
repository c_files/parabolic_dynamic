#!/bin/bash
 
# script file for BASH 
# which bash
# save this file as g.sh
# chmod +x g.sh
# ./g.sh
 

 
# for all ppm files in this directory
for file in *.ppm ; do
  # b is name of file without extension
  b=$(basename $file .ppm)
  # convert from pgm to gif and add text ( level ) using ImageMagic
  convert $file -pointsize 100 -annotate +10+100 $b ${b}.gif
  echo $file
done
 
# convert gif files to animated gif
# convert -resize 800x600 -delay 50  -loop 0 %d.gif[0-1200] a.gif
# if you do not want to use all consecutive files 
convert `ls *.gif | sort -n` -resize 800x600 -delay 50  -loop 0 a.gif 
echo OK
