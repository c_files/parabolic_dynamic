/* 

code from : 
http://mathr.co.uk/blog/2014-11-02_practical_interior_distance_rendering.html
and by other people ;
see description of the functions
 
license GPL3+ <http://www.gnu.org/licenses/gpl.html>


 
---------- how to use ? -----------------------------------------------
to compile with gcc :

 gcc -std=c99 -Wall -Wextra -pedantic -O3 -fopenmp  d.c -lm

./a.out

gcc -std=c99 -Wall -Wextra -pedantic -O3 -fopenmp  d.c -lm

time ./a.out
Text file  saved. 

real	19m4.124s
user	58m2.001s
sys	0m7.987s






------------- gnuplot --------------------
set terminal postscript portrait enhanced mono dashed lw 1 "Helvetica" 14     
set output "data.ps"
set title "Points on dynamical plane "
set ylabel "ratio"
set xlabel "iteratio max"
set xrange [0:150]
plot "data.txt" using 1:2 title 'exterior' lc 1,  "data.txt" using 1:3 title 'interior' lc 2, "data.txt" using 1:4 title 'unknown' lc 3




*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <omp.h> // OpenMP; needs also -fopenmp


 
/* --------------------- global variables --------------- */


const double pi = 3.14159265358979323846264338327950288419716939937510;
const double escape_radius = 2.0 ;
double escape_radius_2 ;  // = escape_radius * escape_radius 
double d2Min; // minimal distance from z to z_alfa 

/* components

white = exterior of Julia set
black = boundary of Julia set ( using DEM )
red = glitches
yellow = known interior of Julia set
blue = unknown 
*/
int iExterior = 0;
int iInterior = 1;
int iUnknown  = 2;

// Number of pixels in the image 
int ExteriorPixels = 0;
int InteriorPixels = 0;
int UnknownPixels  = 0;
double AllPixels; 





/* ------------------ functions --------------------------*/
 
 
/* find c in component of Mandelbrot set 
 
   uses code by Wolf Jung from program Mandel
   see function mndlbrot::bifurcate from mandelbrot.cpp
   http://www.mndynamics.com/indexp.html
 
*/
double complex GiveC(unsigned int numerator, unsigned int denominator, double InternalRadius, unsigned int ParentPeriod)
{
  
  
  double t = 2.0*pi*numerator/denominator; // Internal Angle from turns to radians 
  double R2 = InternalRadius * InternalRadius;
  double Cx, Cy; /* C = Cx+Cy*i */
 
  switch ( ParentPeriod ) // of component 
    {
    case 1: // main cardioid
      Cx = (cos(t)*InternalRadius)/2-(cos(2*t)*R2)/4; 
      Cy = (sin(t)*InternalRadius)/2-(sin(2*t)*R2)/4; 
      break;
    case 2: // only one component 
      Cx = InternalRadius * 0.25*cos(t) - 1.0;
      Cy = InternalRadius * 0.25*sin(t); 
      break;
      // for each ParentPeriod  there are 2^(ParentPeriod-1) roots. 
    default: // higher periods : not works, to do
      Cx = 0.0;
      Cy = 0.0; 
      break; }
 
  return Cx + Cy*I;
}
 
 
/*
 
  http://en.wikipedia.org/wiki/Periodic_points_of_complex_quadratic_mappings
  z^2 + c = z
  z^2 - z + c = 0
  ax^2 +bx + c =0 // ge3neral for  of quadratic equation
  so :
  a=1
  b =-1
  c = c
  so :
 
  The discriminant is the  d=b^2- 4ac 
 
  d=1-4c = dx+dy*i
  r(d)=sqrt(dx^2 + dy^2)
  sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx +- sy*i
 
  x1=(1+sqrt(d))/2 = beta = (1+sx+sy*i)/2
 
  x2=(1-sqrt(d))/2 = alfa = (1-sx -sy*i)/2
 
  alfa : attracting when c is in main cardioid of Mandelbrot set, then it is in interior of Filled-in Julia set, 
  it means belongs to Fatou set ( strictly to basin of attraction of finite fixed point )
 
*/
// uses global variables : 
//  ax, ay (output = alfa(c)) 
double complex GiveAlfaFixedPoint(double complex c)
{
  double dx, dy; //The discriminant is the  d=b^2- 4ac = dx+dy*i
  double r; // r(d)=sqrt(dx^2 + dy^2)
  double sx, sy; // s = sqrt(d) = sqrt((r+dx)/2)+-sqrt((r-dx)/2)*i = sx + sy*i
  double ax, ay;
 
  // d=1-4c = dx+dy*i
  dx = 1 - 4*creal(c);
  dy = -4 * cimag(c);
  // r(d)=sqrt(dx^2 + dy^2)
  r = sqrt(dx*dx + dy*dy);
  //sqrt(d) = s =sx +sy*i
  sx = sqrt((r+dx)/2);
  sy = sqrt((r-dx)/2);
  // alfa = ax +ay*i = (1-sqrt(d))/2 = (1-sx + sy*i)/2
  ax = 0.5 - sx/2.0;
  ay =  sy/2.0;
 
 
  return ax+ay*I;
}
 


 
// https://en.wikipedia.org/wiki/Euclidean_distance
double GiveDistance( complex double z1, complex double z2)
{
  double dx,dy;
 
  dx = creal(z1)-creal(z2);
  dy = cimag(z1)-cimag(z2);
  
  return (sqrt(dx*dx +dy*dy ));
 
}






/* 

code from : 
http://mathr.co.uk/blog/2014-11-02_practical_interior_distance_rendering.html

*/



static inline double cabs2(complex double z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

static inline unsigned char *image_new(int width, int height) {
  return malloc(width * height * 3);
}

static inline void image_delete(unsigned char *image) {
  free(image);
}

static inline void image_save_ppm(unsigned char *image, int width, int height, const char *filename) {
  FILE *f = fopen(filename, "wb");
  if (f) {
    fprintf(f, "P6\n%d %d\n255\n", width, height);
    fwrite(image, width * height * 3, 1, f);
    fclose(f);
  } else {
    fprintf(stderr, "ERROR saving `%s'\n", filename);
  }
}

static inline void image_poke(unsigned char *image, int width, int i, int j, int r, int g, int b) {
  int k = (width * j + i) * 3;
  image[k++] = r;
  image[k++] = g;
  image[k  ] = b;
}





static inline void colour_pixel(unsigned char *image, int width, int i, int j, int component) {
  
  
  int r, g, b;
 // color depends on component
 if (component == iExterior ) { r = 245; g = 245; b = 245; } // light gray
 if (component == iInterior ) { r = 180; g =   180; b = 180; } // dark gray 
 if (component == iUnknown  ) { r = 200; g =   0; b =   0; } // red 
   
  image_poke(image, width, i, j, r, g, b);
}

static inline void render(unsigned char *image, int maxiters, int width, int height,complex double c,  complex double center, double radius, complex double z_alfa) {

  double pixel_spacing = radius / (height / 2.0);
  d2Min = pixel_spacing * 15; 

  #pragma omp parallel for ordered schedule(auto) shared(ExteriorPixels, InteriorPixels, UnknownPixels)  
    for (int j = 0; j < height; ++j) {
     for (int i = 0; i < width; ++i) {
      double x = i + 0.5 - width / 2.0;
      double y = height / 2.0 - j - 0.5;
      complex double z = center + pixel_spacing * (x + I * y);
      int component ;  
      double r2=0.0; //  r = cabs(z) = radius or abs value of complex number 
      double d2 = GiveDistance(z, z_alfa); // d = distance( z, z_alfa)     
      

      if (d2 > d2Min)     
      // iteration 
      for (int n = 1; n <= maxiters; ++n) {
        z = z * z + c;
        r2 = cabs2(z); // r = cabs(z) 
        d2 = GiveDistance(z, z_alfa); // d = distance( z, z_alfa)   
        if (r2 > escape_radius_2) break; 
        if (d2 < d2Min) break;   
       }

     
          if (r2 > escape_radius_2) {component= iExterior; 
                                      #pragma omp atomic
                                     ExteriorPixels++; } 
         else { if  (d2 < d2Min) { component= iInterior; 
                                    #pragma omp atomic
                                   InteriorPixels++;}
               else {component= iUnknown; 
                                  #pragma omp atomic
                                  UnknownPixels++;} }    
     colour_pixel(image, width, i, j, component);
    } 
  }
}








int main() {
   
  int maxmaxiter =1200; //maximal number of iterations : z(i+1) = fc( zi)
  
  // parameter of the function fc(z)= z*z +c 
  complex double c = -0.75;
  
  // image : 
  // integer sizes of the image 
  int width = 2000;
  int height = 1500;
  // description of the image : center and radius 
  complex double zcenter = 0.0;
  double radius = 1.3; 




 
  
  unsigned int length = 30;
  // text file name for data about images 
  // compare with http://mathr.co.uk/blog/2014-11-02_practical_interior_distance_rendering.html

 AllPixels= width*height; 
  FILE * fp;     
  fp = fopen("data.txt","wb"); /*create new file,give it a name and open it in binary mode  */
  fprintf(fp,"# maxiter ExteriorPixels InteriorPixels UnknownPixesl\n");  /*writ`e header to the file*/



  complex double z_alfa;  // alfa fixed point alfa = f(alfa)

  // setup 
  z_alfa = GiveAlfaFixedPoint(c); 
  escape_radius_2 = escape_radius * escape_radius ; 

  for (int maxiter = 0; maxiter <= maxmaxiter; ++maxiter) {
         // filename from maxiter
         char filename[length] ;
         snprintf(filename, length,  "%d.ppm", maxiter);
         // setup 
         unsigned char *image = image_new(width, height);
         // render image and save 
         render(image, maxiter, width, height, c, zcenter, radius, z_alfa);
         image_save_ppm(image, width, height, filename);
         // free image 
         image_delete(image);
         // save info about image   
         fprintf(fp," %d %f %f  %f\n", maxiter, ExteriorPixels/AllPixels, InteriorPixels/AllPixels, UnknownPixels/AllPixels);
        // printf(" %d ; %d ; %d ;  %d ; %d ; %.0f\n", maxiter,ExteriorPixels, InteriorPixels, UnknownPixels, ExteriorPixels+ InteriorPixels+ UnknownPixels,  AllPixels);
         ExteriorPixels = 0;
         InteriorPixels = 0;
         UnknownPixels  = 0;  
 }

  printf("Text file  saved. \n");
  fclose(fp);

  return 0;
}
